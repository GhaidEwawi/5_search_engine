from bs4 import BeautifulSoup
import requests
from text import Text
from website import Website

class linkObject :
    def __init__(self, linkURL, depth, parent):
        self.url = linkURL
        self.depth = depth
        self.parent = parent
        self.children = []

class Crawler:

    visited = {}
    id = 2

    def __init__(self):
        pass

    def crawlWesbite(self, website, depth=0):
        """
        Takes a website and gets all the important texts and adds them to the database, then it crawls inside the children of that
        website. Currently it crawls into the depth of 2
        :param: website: The website we want to scrap
        :param: depth: The current depth of the crawling Tree, default value is zero
        """
        if depth == 2:
            return

        # Checks if the crawler visited the website before and then mark it as visited. This stops circular visit.
        if website in self.visited.keys():
            return
        self.visited[website] = self.id

        # Get all the links inside the current website to crawl them.
        nextWebsites = self.crawlPage(website)

        # Crawl all links inside the current page.
        for childWebsite in nextWebsites:
            self.crawlWesbite(childWebsite, depth+1)

        self.id += 1

        # This is excuted after the crawler finishes its work and adds all websites to the database.
        if depth == 0:
            self.addLinksToDatabase()


    def addLinksToDatabase(self):
        websites = []
        for url, id in self.visited.items():
            tempWebsite = Website.create({'website_url': url})
            websites.append(tempWebsite)

        for website in websites:
            website.save('website_url')


    def crawlPage(self, link):
        website = link
        response = requests.get(website)
        soup = BeautifulSoup(response.text, 'html.parser')

        # Find inner links
        childrenLinks = self.findChildrenUrls(soup)

        # Look for the important tags inside the page: h1-h6, strong, b, p
        elements = self.getImportantElements(soup)

        # An array to conrain results
        textElements = []

        # Get the id of the website.
        website_id = self.id

        # Extract the text from the htmlElement and add it to a textElements list.
        for element in elements:
            for result in element:
                s = result.get_text()
                s = self.cleanString(s)
                importance = self.findImportance(result.name)
                tempText = Text.create({'website_id': website_id, 'text': s, 'importance': importance})
                textElements.append(tempText)

        # Save all text elements to the database
        for item in textElements:
            item.save('text')
        return childrenLinks

    def getImportantElements(self, soup):
        x = []
        x.append(soup.find_all("h1"))
        x.append(soup.find_all("h2"))
        x.append(soup.find_all("h3"))
        x.append(soup.find_all("h4"))
        x.append(soup.find_all("h5"))
        x.append(soup.find_all("h6"))
        x.append(soup.find_all("strong"))
        x.append(soup.find_all("b"))
        x.append(soup.find_all("p"))
        return x

    def findChildrenUrls(self, soup):
        attribute_tags = soup.find_all('a')
        links = []
        for attribute_tag in attribute_tags:
            if attribute_tag.has_attr('href'):
                link = attribute_tag['href']
                if self.is_valid_url(link):
                    links.append(link)

                    return links

    @staticmethod
    def addToSeen(linkObj):
        seenLinks[linkObj.url] = linkObj;

    # Returns whether the link has been seen.
    @staticmethod
    def linkInSeenListExists(linkObj):
        return seenLinks[linkObj.url] == null


    @staticmethod
    def is_valid_url(url):
        import re
        regex = re.compile(
            r'^https?://'  # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'  # domain...
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        return url is not None and regex.search(url)


    @staticmethod
    def findImportance(tag_name):
        importances = {
            'h1': 15,
            'h2': 12,
            'h3': 10,
            'h4': 8,
            'h5': 6,
            'h6': 4,
            'p': 3,
            'b': 5,
            'strong': 10
        }

        return importances[tag_name]

    @staticmethod
    def cleanString(temp_string):
        clean_string = temp_string.replace('\'', '')
        clean_string = clean_string.replace('\"', '')

        return clean_string
