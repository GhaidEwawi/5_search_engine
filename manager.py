import config
from mysql.connector import Error
import mysql.connector

from db_result_set import DbResultSet

class DbManager:
    def __init__(self, host=None, user=None, password=None, database=None):
        self.host = host if host is not None else config.host
        self.user = user if user is not None else config.user
        self.password = password if password is not None else config.password
        self.database = database if database is not None else config.database
        DbManager.connection = self.connect()

    def connect(self):
        connection = None

        try:
            connection = mysql.connector.connect(
              host=self.host,
              user=self.user,
              password=self.password,
              database=self.database
            )
        except Error as e:
            print("Error while connecting to MySQL", e)

        return connection

    def execute(self, query):
        """
        Executes the given SQL statement(s) on the database, and returns ID of the last inserted row or the number of
        rows that were affected.
        :param query: string|list of string
        :return: bool
        """
        connection = DbManager.connection
        cursor = connection.cursor()
        result = False

        try:
            cursor.execute(query)
            connection.commit()
            # If we have inserted, we get the latest inserted ID, otherwise we get 1 to indicate that the query
            # has succeeded no matter how many rows were affected.
            result = cursor.lastrowid if cursor.lastrowid > 0 else 1



        except Error as e:
            print("Error ", query, e)

            try:
                connection.rollback()
            except Error as e:
                print("Error ", query, e)

        finally:
            cursor.close()
            del cursor

        return result


    def executeSelectQuery(self, query):
        """
        Executes the given SQL select query on the database, and returns the whole result set, or None.
        :param query: string
        :return: DbResultSet
        """
        resultSet = DbResultSet()
        connection = DbManager.connection
        cursor = connection.cursor()

        try:
            cursor.execute(query)
            rows = cursor.fetchall()
            resultSet.fill(rows)

        except Error as e:
            print('Error ', query, e)

        finally:
            cursor.close()
            del cursor

        return resultSet
