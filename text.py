from model import DbModel
class Text(DbModel):

    _dbTable = 'texts'
    _fields = ['website_id', 'text', 'importance']

    @classmethod
    def create(self, fieldsValuesDict={}):
        """
        Creates an instance of the calling class.
        :return:
        """
        instance = Text()

        for (field, value) in fieldsValuesDict.items():
            setattr(instance, field, value)
        return instance

    def __str__(self):
        temp = ""
        temp += f'Table Name : {self._dbTable}\n'
        temp += str(self.__dict__)
        return temp
