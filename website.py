from model import DbModel
class Website(DbModel):

    _dbTable = 'websites'
    _fields = ['website_url']

    @classmethod
    def create(self, fieldsValuesDict={}):
        """
        Creates an instance of the calling class.
        :return:
        """
        instance = Website()

        for (field, value) in fieldsValuesDict.items():
            setattr(instance, field, value)
        return instance

    def __str__(self):
        temp = ""
        temp += f'Table Name : {self._dbTable}\n'
        temp += str(self.__dict__)
        return temp
